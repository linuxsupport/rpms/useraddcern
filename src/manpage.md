% USERADDCERN(1)
% Jaroslaw Polok <jaroslaw.polok@cern.ch>, Ben Morrice <ben.morrice@cern.ch>
% October 2023

# NAME

addusercern - Utility to add CERN AFS accounts to the system.

# DESCRIPTION

addusercern (also known as useraddcern) is a command line tool to ease adding
of CERN accounts to local system setup.
It will search CERN LDAP database for user and group information and 
add system accounts according to search criteria defined.

# SYNOPSIS

addusercern \[\-\-help\]

addusercern \[\-\-shell\] \[\-\-directory\] \[\-\-login\] LOGINID \[.. LOGINID\]

# OPTIONS

 **\-\-help**

    Shows this help desription

 **\-\-login** LOGINID

    Add user with given LOGINID

 **\-\-shell**

    Install user default shell if not present

 **\-\-directory**

    Use the _unixHomeDirectory_ configured in LDAP (usually AFS or EOS). The default is to
    configure a local /home homedirectory

All options can be abbreviated to shortest distinctive length,
(first letter). Single minus preceding option name may be used 
instead of double one.

For wildcard search use \* character (double quoted or back-slashed 
to avoid shell variable expansion: "\*" or \\\* ).

This tool will also query the **/etc/sysconfig/locmap-initialsetup** file for
the variable **LOCMAP\_HOMEDIRECTORY\_LOCAL**. Should this file and variable exist,
the 'default' behaviour of \-\-directory will be what is defined for the above
variable.
If \-\-directory is passed on the command line, the above logic is NOT
executed and any configuration stored in '/etc/sysconfig/locmap-initialsetup'
will be ignored.

Please note that search option arguments are case insensitive.

# DESCRIPTION

addusercern performs a search in CERN LDAP database, according to 
search criteria defined by options and adds named users to the system.

# EXAMPLES

useraddcern --login jpolok

useraddcern -l "\*polok"

# KNOWN BUGS

No strict checking of adduser/addgroup return and/or output ... to be fixed.

Also we are eager to hear about any (other) bugs at linux.support@cern.ch

Number of returned results is limited to 2000. (this is rather a feature).
