%define debug_package %{nil}

Summary: CERN LDAP user/group database client.
Name: useraddcern
Version: 1.2
Release: 1%{?dist}
Source0: %{name}-%{version}.tgz
Group: CERN/Utilities
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Vendor:  CERN
License: GPL

Requires: shadow-utils >= 4.0.17
%if 0%{?rhel} == 7
BuildRequires: perl
Requires: perl
Requires: perl-LDAP
%endif
%if 0%{?rhel} >= 8
Requires: python3-ldap
%endif
# pandoc is used to generate the manpage from something readable (markdown)
BuildRequires: pandoc
BuildArch: noarch

%description
useraddcern is the CERN LDAP user/group database client.
It acts as an frontend to system useradd / groupadd commands
and allows easy configuration of CERN AFS accounts on the system.

%prep
%setup

%build
pandoc -s -t man manpage.md > %{name}.1
cp %{name}.1 addusercern.1

%install
mkdir -p $RPM_BUILD_ROOT/%{_sbindir}
%if 0%{?rhel} == 7
install -m 755 %{name}.pl $RPM_BUILD_ROOT/%{_sbindir}/%{name}
%endif
%if 0%{?rhel} >= 8
install -m 755 %{name}.py $RPM_BUILD_ROOT/%{_sbindir}/%{name}
%endif
cd $RPM_BUILD_ROOT/%{_sbindir}
/bin/ln -sf %{name} addusercern
cd -
mkdir -p $RPM_BUILD_ROOT/%{_mandir}/man1
install -m 644 %{name}.1 $RPM_BUILD_ROOT/%{_mandir}/man1/
install -m 644 addusercern.1 $RPM_BUILD_ROOT/%{_mandir}/man1/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README
%{_sbindir}/%{name}
%{_sbindir}/addusercern
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/addusercern.1*

%changelog
* Wed Oct 11 2023 Ben Morrice <ben.morrice@cern.ch> 1.2-1
- respect LOCMAP_HOMEDIRECTORY_LOCAL variable in
  /etc/sysconfig/locmap-initialsetup (if present)
- Add switches for more complex home area management 
- Move away from pod2man to pandoc for man page generation

* Wed Apr 05 2023 Ben Morrice <ben.morrice@cern.ch> 1.1-1
- use correct flag for no-user-group (-N vs -n)

* Fri Dec 09 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- add a flag to permit forcing primary GID (python port)

* Wed Nov 09 2022 Ben Morrice <ben.morrice@cern.ch> 0.9
- add an --update argument on the python port

* Mon Oct 31 2022 Jan Iven <jan.iven@cern.ch> 0.8
- make local homedirectories the default

* Mon Oct 10 2022 Manuel Guijarro <manuel.guijarro@cern.ch> 0.7
- allow apostrophe in gecos

* Wed Jul 20 2022 Manuel Guijarro <manuel.guijarro@cern.ch> 0.6
- add option "-s" to install user default shell if not present

* Thu Jun 02 2022 Ben Morrice <ben.morrice@cern.ch> 0.5.2-1
- ensure that uid from ldap is passed to useradd

* Wed May 25 2022 Ben Morrice <ben.morrice@cern.ch> 0.5.1-1
- Ensure python port works without requiring --login

* Fri Apr 29 2022 Ben Morrice <ben.morrice@cern.ch> 0.5-1
- port from perl to python (for el8+)

* Thu Nov 14 2019 Ben Morrice <ben.morrice@cern.ch> 0.4-2
- build on el8

* Mon Feb 13 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.4-1
- added warning if shell is not installed on system.

* Wed Jul 11 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0.2-1
- added exit code (1 on failures)

* Mon Feb  8 2010 KELEMEN Peter <Peter.Kelemen@cern.ch> - 0.1-1
- cleaned up packaging
* Wed May 27 2009 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0-2
- changed to use xldap.cern.ch
* Tue Oct 28 2008 Jaroslaw Polok <jaroslaw.polok@cern.ch> 0-1
- initial build
